//
//  FlightDataViewController.m
//  SFOBeacon
//
//  Created by Kalvin Loc on 6/14/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import "FlightDataViewController.h"

@interface FlightDataViewController () <NSURLConnectionDelegate>

@property NSMutableData* SOAPDATA;

@end

@implementation FlightDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString* flight = @"ANA8";
    
    NSString *sSOAPMessage = @"<OTA_AirFlifoRQ Version=\"2.0.0\"><OriginDestinationInformation><FlightSegment FlightNumber=\"8\"><MarketingAirline Code=\"ANA\" FlightNumber=\"8\"/></FlightSegment></OriginDestinationInformation></OTA_AirFlifoRQ>";
    NSData* SOAPDATA;
    
    NSURL *sRequestURL = [NSURL URLWithString:@"https://sws3-crt.cert.sabre.com"];
    NSMutableURLRequest *myRequest = [NSMutableURLRequest requestWithURL:sRequestURL];
    NSString *sMessageLength = [NSString stringWithFormat:@"%d", [sSOAPMessage length]];
    
    [myRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [myRequest addValue: @"https://sws3-crt.cert.sabre.com/OTA_AirFlifoLLSRQ" forHTTPHeaderField:@"SOAPAction"];
    [myRequest addValue: sMessageLength forHTTPHeaderField:@"Content-Length"];
    [myRequest setHTTPMethod:@"POST"];
    [myRequest setHTTPBody: [sSOAPMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:myRequest delegate:self];
    
    if( theConnection ) {
        SOAPDATA = [NSMutableData data];
    }else {
        NSLog(@"Some error occurred in Connection");
        
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    //[self.SOAPDATA  setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.SOAPDATA  appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Some error in your Connection. Please try again.");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"Received Bytes from server: %d", [self.SOAPDATA length]);
    NSString *myXMLResponse = [[NSString alloc] initWithBytes: [self.SOAPDATA bytes] length:[self.SOAPDATA length] encoding:NSUTF8StringEncoding];
    NSLog(@"%@",myXMLResponse);
}

@end

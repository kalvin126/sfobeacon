//
//  Terminal2.h
//  SFOBeacon
//
//  Created by Ethan Gates on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegisteredBeacon.h"

@interface Terminal2 : NSObject

+ (id) shared;

@property NSMutableArray * allBeacons;
@property NSMutableDictionary * beaconsByZone;

- (NSArray*)allBusinessBeacons; //registered ones that is

- (RegisteredBeacon*)getBeaconByZoneID:(NSInteger)ID;

@end

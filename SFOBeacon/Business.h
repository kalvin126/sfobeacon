//
//  Business.h
//  SFOBeacon
//
//  Created by Connie Lim on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Business : NSObject


@property NSString* title;
@property NSString* hours;
@property NSString* location;
@property NSString* body;
@property NSString* email;
@property NSString* phone;
@property NSString* location_summary;

@property NSURL* image;
@property NSURL* map;
@property NSURL* web;

@property NSInteger nid;

- (id) initWithData:(NSDictionary*) dict;

@end
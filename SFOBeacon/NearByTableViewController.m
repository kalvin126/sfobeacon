//
//  NearByTableViewController.m
//  SFOBeacon
//
//  Created by Kalvin Loc on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import "NearByTableViewController.h"
#import "Terminal2.h"

@interface NearByTableViewController ()


@property NSDictionary* zones;
@property NSArray* nearBeacons;

@end

@implementation NearByTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self buildIDSZones];
    [self searchNearbyBeacons];
}

- (void)buildIDSZones{
    IDSBuilding* building = [[self surfaceVC] building];
    NSMutableDictionary* zones = [[NSMutableDictionary alloc] init];
    
    for(int i = 0; i<2; i++){
        IDSFloor* floor = [building floorAtLevel:i+1];
        for(IDSZone* zone in [floor zones]){
            NSMutableArray* points = [zone points];
            NSInteger x = 0, y = 0;
            for(IDSZonePoint* point in points){
                x += point.x;
                y += point.y;
            }
            x = x/[points count];
            y = y/[points count];
            
            NSDictionary* avgPoint = @{@"x" : [NSNumber numberWithInteger:x],
                                       @"y" : [NSNumber numberWithInteger:y]
                                       };
            
            [zones setValue:avgPoint forKey:[zone name]];
        }
    }
    
    [self setZones:[zones copy]];
}

- (NSArray*)searchNearbyBeacons{
    IDSCoordinate* userCord = [[[[self surfaceVC] surfaceView] mapScrollView] userCurrentLocation];
    NSInteger distanceThreshold = 12192; // 40 feet
    NSMutableArray* closest = [[NSMutableArray alloc] init];

    NSArray* zoneIDS = [[self zones] allKeys];
    for(NSString* zoneID in zoneIDS){
        NSDictionary* zone = [[self zones] objectForKey:zoneID];
        
        NSInteger x = pow(([zone[@"x"] integerValue] + [userCord x]) , 2);
        NSInteger y = pow(([zone[@"y"] integerValue] + [userCord y]) , 2);
        
        NSInteger dist = sqrt(abs(x+y));
        
        if(dist <= distanceThreshold){
            NSDictionary* node = @{@"zoneID" : zoneID,
                                   @"distance" : [NSNumber numberWithInteger:dist]
                                   };
            [closest addObject:node];
        }
    }
    
    NSLog(@"Items close: %ld", (unsigned long)[closest count]);

    NSArray* sortedArray = [closest sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber* first = [(NSDictionary*)a objectForKey:@"distance"];
        NSNumber* second = [(NSDictionary*)b objectForKey:@"distance"];
        return [first compare:second];
    }];
    
    [self setNearBeacons:sortedArray];
    return sortedArray;
}

- (IBAction)pressedClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)pressedRefresh:(id)sender {
    [self searchNearbyBeacons];
    [[self tableView] reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    NSInteger ret = ([[self nearBeacons] count] );
    if(ret > 0)
        return 1;
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self nearBeacons] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NearByReuse" forIndexPath:indexPath];

    NSDictionary* zone = [[self nearBeacons] objectAtIndex:[indexPath row]];
    IDSZone* stockZone = [[[self surfaceVC] building] getZoneById:[[zone objectForKey:@"zoneID"] integerValue]];
    
    RegisteredBeacon* rBeacon = [[Terminal2 shared] getBeaconByZoneID:[[zone objectForKey:@"zoneID"] integerValue]];
    
    cell.textLabel.text = [rBeacon name];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%f ft", [[zone objectForKey:@"distance"] integerValue]*0.00328084];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary* zone = [[self nearBeacons] objectAtIndex:[indexPath row]];
    
    IDSCoordinate* start = [[[[self surfaceVC] surfaceView] mapScrollView] userCurrentLocation];;
    IDSCoordinate* end = [[IDSCoordinate alloc] initWithX:[[zone objectForKey:@"x"] integerValue] andY:[[zone objectForKey:@"y"] integerValue] andFloorLevel:1];

    [[Indoors instance] routeFromLocation:start toLocation:end delegate:[self map]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSString* identifier = [segue identifier];
    
    if([identifier isEqualToString:@""]){
    }
    
}


@end

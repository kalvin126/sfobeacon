//
//  BusinessViewController.h
//  SFOBeacon
//
//  Created by Connie Lim on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessViewController : UIViewController

@property NSDictionary* business;

@end

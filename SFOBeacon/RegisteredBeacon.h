//
//  RegisteredBeacon.h
//  Pods
//
//  Created by Ethan Gates on 6/13/15.
//
//

#import <Foundation/Foundation.h>

@interface RegisteredBeacon : NSObject

+(instancetype) beaconWithRow:(NSString*) line;

-(BOOL) isBusiness;
@property (readonly) NSString * poi;
@property (readonly) NSString * category;
@property (readonly) NSString * subCategory;
@property (readonly) NSString * subSubCategory;
@property (readonly) NSString * name;
@property (readonly) NSString * shortDescription;
@property (readonly) NSString * longDescription; // not actually long, just "description" is shadowed in NSObject
@property (readonly) NSNumber * zoneRefID;
@property (readonly) NSString * hours; //(business)
@property (readonly) NSArray * tags;

@end

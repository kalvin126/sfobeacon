//
//  CategoryTableViewController.h
//  SFOBeacon
//
//  Created by Kalvin Loc on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewController : UITableViewController

@property NSArray* catagory;
@property NSString* name;

@end

//
//  DirectoryTableViewController.m
//  SFOBeacon
//
//  Created by Kalvin Loc on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import "DirectoryTableViewController.h"
#import "CategoryTableViewController.h"

@interface DirectoryTableViewController ()

@property NSArray* airlines;
@property NSArray* dining;
@property NSArray* shopping;
@property NSArray* thingsToDo;
@property NSArray* passengerServices;

@end

@implementation DirectoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self reloadData];
}

- (NSURL*) generateAPIURLforService:(NSString*) service{
    NSString* APIKey = @"8e5f14641b90b2e887bf1b84ff8b2e89";
    NSURL* final = [NSURL URLWithString:[@"" stringByAppendingFormat:@"http://www.flysfo.com/api/%@.json?limit=20&key=%@", service, APIKey]];
    
    return final;
}

- (void) reloadData{
    // Airlines
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[self generateAPIURLforService:@"airlines"]] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(error){
            NSLog(@"Airlines Error: %@", error);
        }else{
            NSLog(@"Airlines Recieved");
            [self setAirlines:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
            [self reloadTable];
        }
    }];
    // Dining
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[self generateAPIURLforService:@"dining"]] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(error){
            NSLog(@"Dining Error: %@", error);
        }else{
            NSLog(@"Dining Recieved");
            [self setDining:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
            [self reloadTable];
        }
    }];
    
    // Shopping
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[self generateAPIURLforService:@"shopping"]] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(error){
            NSLog(@"Shopping Error: %@", error);
        }else{
            NSLog(@"Shopping Recieved");
            [self setShopping:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
            [self reloadTable];
        }
    }];
    
    // thingsToDo
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[self generateAPIURLforService:@"things-to-do"]] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(error){
            NSLog(@"thingsToDo Error: %@", error);
        }else{
            NSLog(@"thingsToDo Recieved");
            [self setThingsToDo:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
            [self reloadTable];
        }
    }];
    
    // passengerServices
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:[self generateAPIURLforService:@"passenger-services"]] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(error){
            NSLog(@"passengerServices Error: %@", error);
        }else{
            NSLog(@"passengerServices Recieved");
            [self setPassengerServices:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
            [self reloadTable];
        }
    }];
}

- (void) reloadTable{
    
    if([[self airlines] count] && [[self dining] count] && [[self shopping] count] && [[self thingsToDo] count] && [[self passengerServices] count]){
        NSLog(@"Table Reloaded");
        [[self tableView] reloadData];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    
    if([[self passengerServices] count] !=0)
        return 1;
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"directoryReuse" forIndexPath:indexPath];
    
    // Configure the cell...
    NSString* category;
    NSString* count;
    
    if([indexPath row] == 0){
        category = @"Airlines";
        count = [NSString stringWithFormat:@"%ld", (unsigned long)[[self airlines] count]];
    }else if([indexPath row] == 1){
        category = @"Dining";
        count = [NSString stringWithFormat:@"%ld", (unsigned long)[[self dining] count]];
    }else if([indexPath row] == 2){
        category = @"Shopping";
        count = [NSString stringWithFormat:@"%ld", (unsigned long)[[self shopping] count]];
    }else if([indexPath row] == 3){
        category = @"Things To Do";
        count = [NSString stringWithFormat:@"%ld", (unsigned long)[[self thingsToDo] count]];
    }else if([indexPath row] == 4){
        category = @"Passenger Services";
        count = [NSString stringWithFormat:@"%ld", (unsigned long)[[self passengerServices] count]];
    }
    
    cell.textLabel.text = category;
    cell.detailTextLabel.text = count;
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSString* identifier = [segue identifier];
    NSIndexPath* selectedRow = [[self tableView] indexPathForSelectedRow];
    NSArray* category;
    NSString* name;
    
    if([identifier isEqualToString:@"categorySegue"]){
        if([selectedRow row] == 0){
            name = @"Airline";
            category = [self airlines];
        }else if([selectedRow row] == 1){
            name = @"Dining";
            category = [self dining];
        }else if([selectedRow row] == 2){
            name = @"Shopping";
            category = [self shopping];
        }else if([selectedRow row] == 3){
            name = @"Things To Do";
            category = [self thingsToDo];
        }else if([selectedRow row] == 4){
            name = @"Passenger Services";
            category = [self passengerServices];
        }
        
        [((CategoryTableViewController*)[segue destinationViewController]) setTitle:name];
        [((CategoryTableViewController*)[segue destinationViewController]) setCatagory:category];
    }
    
}

@end

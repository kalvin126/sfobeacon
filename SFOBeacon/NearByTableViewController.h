//
//  NearByTableViewController.h
//  SFOBeacon
//
//  Created by Kalvin Loc on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Indoors/Indoors.h>
#import <IndoorsSurface/ISIndoorsSurfaceViewController.h>
#import "MapViewController.H"

@interface NearByTableViewController : UITableViewController

@property ISIndoorsSurfaceViewController* surfaceVC;
@property MapViewController* map;

@end

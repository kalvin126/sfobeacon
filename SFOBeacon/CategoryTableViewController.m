//
//  CategoryTableViewController.m
//  SFOBeacon
//
//  Created by Kalvin Loc on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import "CategoryTableViewController.h"
#import "BusinessViewController.h"

@interface CategoryTableViewController ()

@end

@implementation CategoryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [[self catagory] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"catagoryReuse" forIndexPath:indexPath];
    
    NSDictionary* business = [[self catagory] objectAtIndex:[indexPath row]];
    // Configure the cell...
    cell.textLabel.text = [business objectForKey:@"node_title"];
    //cell.detailTextLabel.text = [business valueForKey:@"hours"];
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath* selected = [[self tableView] indexPathForSelectedRow];
    NSDictionary* business = [[self catagory] objectAtIndex:[selected row]];
    NSString* ident = [segue identifier];
    if([ident isEqualToString:@"BusinessSegue"]){
        BusinessViewController* dest = (BusinessViewController*)[segue destinationViewController];
        
        [dest setBusiness:business];
    }
    
    
}

@end

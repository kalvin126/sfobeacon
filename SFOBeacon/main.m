//
//  main.m
//  SFOBeacon
//
//  Created by Kalvin Loc on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

//
//  Business.m
//  SFOBeacon
//
//  Created by Connie Lim on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import "Business.h"

@implementation Business

- (id) initWithData:(NSDictionary *)dict{
    self.title = [dict objectForKey:@"title"];
    self.hours = dict[@"hours"];
    self.location = dict[@"location"];
    self.body = dict[@"body"];
    self.email = dict[@"email"];
    self.phone = dict[@"phone"];
    self.image = dict[@"image"];
    self.map = dict[@"map"];
    self.web = dict[@"web"];
    self.nid = (NSInteger)dict[@"nixd"];
    self.location_summary = dict[@"location_summary"];
    return self;
}

@end

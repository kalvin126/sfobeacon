//
//  MapViewController.m
//  SFOBeacon
//
//  Created by Kalvin Loc on 6/14/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import "MapViewController.h"
#import "NearByTableViewController.h"
#import <Indoors/Indoors.h>
#import <IndoorsSurface/ISIndoorsSurfaceViewController.h>

@interface MapViewController () <IndoorsServiceDelegate, ISIndoorsSurfaceViewControllerDelegate, RoutingDelegate>

@property Indoors* indoor;

@end

@implementation MapViewController {
    ISIndoorsSurfaceViewController *_indoorsSurfaceViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    __unused Indoors *indoors = [[Indoors alloc] initWithLicenseKey:@"b9913f36-6a12-4eba-b751-568fd3fb6fe4" andServiceDelegate:self];
    [self setIndoor:indoors];
    
    _indoorsSurfaceViewController = [[ISIndoorsSurfaceViewController alloc] init];
    _indoorsSurfaceViewController.delegate = self;
    
    [self addSurfaceAsChildViewController];
    NSNumber* buildingID = [[NSNumber alloc] initWithLongLong:143183695];
    
    [_indoorsSurfaceViewController loadBuildingWithBuildingId:[buildingID integerValue]];
}

- (void)addSurfaceAsChildViewController{
    [self addChildViewController:_indoorsSurfaceViewController];
    _indoorsSurfaceViewController.view.frame = self.view.frame;
    [self.view addSubview:_indoorsSurfaceViewController.view];
    [_indoorsSurfaceViewController didMoveToParentViewController:self];
}

#pragma mark - ISIndoorsSurfaceViewControllerDelegate

- (void)indoorsSurfaceViewController:(ISIndoorsSurfaceViewController *)indoorsSurfaceViewController isLoadingBuildingWithBuildingId:(NSUInteger)buildingId progress:(NSUInteger)progress{
    NSLog(@"Building loading progress: %lu", (unsigned long)progress);
}

- (void)indoorsSurfaceViewController:(ISIndoorsSurfaceViewController *)indoorsSurfaceViewController didFinishLoadingBuilding:(IDSBuilding *)building{
    NSLog(@"Building loaded successfully!");
}

- (void)indoorsSurfaceViewController:(ISIndoorsSurfaceViewController *)indoorsSurfaceViewController didFailLoadingBuildingWithBuildingId:(NSUInteger)buildingId error:(NSError *)error{
    NSLog(@"Loading building failed with error: %@", error);
    
    [[[UIAlertView alloc] initWithTitle:error.localizedDescription message:error.localizedFailureReason delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok", nil] show];
}

#pragma mark - IndoorsServiceDelegate

- (void)connected{
}

- (void)onError:(IndoorsError *)indoorsError{
}

- (void)locationAuthorizationStatusDidChange:(IDSLocationAuthorizationStatus)status{
}

- (void)bluetoothStateDidChange:(IDSBluetoothState)bluetoothState{
}

#pragma mark - RoutingDelegate

- (void)setRoute:(NSArray *)path{
    [[_indoorsSurfaceViewController surfaceView] showPathWithPoints:path];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString* identifier = [segue identifier];
    
    if([identifier isEqualToString:@"showNearSegue"]){
        NearByTableViewController* dest = (NearByTableViewController*)([(UINavigationController*)[segue destinationViewController] topViewController]);
        [dest setSurfaceVC:_indoorsSurfaceViewController];
        [dest setMap:self];
    }
}


@end

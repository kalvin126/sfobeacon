//
//  Terminal2.m
//  SFOBeacon
//
//  Created by Ethan Gates on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import "Terminal2.h"
@interface Terminal2()

@property NSString* masterString;



@end

@implementation Terminal2

#pragma mark Singleton Methods

+ (id)shared {
    static Terminal2 *oneObject = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
            oneObject = [[self alloc] init];
    });
    return oneObject;
}

- (id)init {
    if (self = [super init]) {
        NSString * path = [[NSBundle mainBundle] pathForResource:@"sfo_master.xlsx - Sheet1" ofType:@".txt"];
        self.masterString = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:NULL];
        
        self.allBeacons = [[NSMutableArray alloc] init];
        self.beaconsByZone = [[NSMutableDictionary alloc]init];
        
        [self createBeaconsFromMaster:self.masterString];
    }
    return self;
}

-(void) createBeaconsFromMaster:(NSString*) hugeString {
    NSArray * rows = [hugeString componentsSeparatedByString:@"\n"];
    rows = [rows subarrayWithRange:NSMakeRange(1, rows.count-1)];
    for (NSString * row in rows) {
        RegisteredBeacon* beacon = [RegisteredBeacon beaconWithRow:row];
        [self.allBeacons addObject:beacon];
        [self addBeaconToDictionary:beacon];
    }
}

-(void) addBeaconToDictionary:(RegisteredBeacon*) beacon {
    NSNumber * key = beacon.zoneRefID;
    if (self.beaconsByZone[key]) {
        [self.beaconsByZone[key] addObject:beacon];
    } else {
        [self.beaconsByZone setObject:[[NSMutableArray alloc]init] forKey:key];
        [[self.beaconsByZone objectForKey:key] addObject:beacon];
    }
}

-(NSArray *)allBusinessBeacons {
    NSMutableArray * busy = [[NSMutableArray alloc]init];
    
    for (RegisteredBeacon* beacon in self.allBeacons) {
        if (beacon.isBusiness) {
            [busy addObject:beacon];
        }
    }
    return busy;
}

- (RegisteredBeacon*)getBeaconByZoneID:(NSInteger)ID{
    for(RegisteredBeacon* beacon in [self allBeacons])
        if([[beacon zoneRefID] integerValue] == ID)
            return beacon;
    
    return nil;
}

@end





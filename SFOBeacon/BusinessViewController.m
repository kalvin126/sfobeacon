//
//  BusinessViewController.m
//  SFOBeacon
//
//  Created by Connie Lim on 6/13/15.
//  Copyright (c) 2015 Kalvin Loc. All rights reserved.
//

#import "BusinessViewController.h"

@interface BusinessViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *Image;
@property (strong, nonatomic) IBOutlet UILabel *displayHours;
@property (strong, nonatomic) IBOutlet UILabel *displayTerminal;
@property (strong, nonatomic) IBOutlet UILabel *displayLocation_summary;
@property (strong, nonatomic) IBOutlet UILabel *displayDescription;
@property (strong, nonatomic) IBOutlet UILabel *displayUrl;
@property (strong, nonatomic) IBOutlet UILabel *displayPhone;
@property (strong, nonatomic) IBOutlet UIScrollView *tempScrollView;

@end

@implementation BusinessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.tempScrollView=(UIScrollView *)self.view;
//    self.tempScrollView.contentSize=CGSizeMake(320,313);
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t bg_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    __block UIImage* img = nil;
    dispatch_group_async(group, bg_queue, ^{
        NSString* url = [[self business] objectForKey:@"image"];
        NSURL* finURL;
        if([url characterAtIndex:0] == '/'){
            finURL = [NSURL URLWithString:[NSString stringWithFormat:@"http:%@", url]];
        }else{
            finURL = [NSURL URLWithString:url];
        }
         NSData *data = [NSData dataWithContentsOfURL:finURL];
        img = [[UIImage alloc] initWithData:data];
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [[self Image] setImage:img];
    });
    
    self.displayHours.text = [[self business] objectForKey:@"hours"];
    self.displayTerminal.text = [[self business] objectForKey:@"terminal"];
    self.displayLocation_summary.text = [[self business] objectForKey:@"location_summary"];
    self.displayDescription.text = [[self business] objectForKey:@"body"];
    self.displayUrl.text = [[self business] objectForKey:@"url"];
    self.displayPhone.text = [[self business] objectForKey:@"phone"];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

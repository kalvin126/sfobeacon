//
//  RegisteredBeacon.m
//  Pods
//
//  Created by Ethan Gates on 6/13/15.
//
//

#import "RegisteredBeacon.h"

@implementation RegisteredBeacon

+ (instancetype)beaconWithRow:(NSString *)line {
    return [[RegisteredBeacon alloc] initWithRow:line];
}

- (BOOL)isBusiness {
    return self.hours.length;
}

-(instancetype) initWithRow:(NSString*) line {
    self = [super init];
    if(self){
        //line is guaranteed to have comma separated values
        // any semicolons used to be commas
        NSArray *fields = [line componentsSeparatedByString: @","];
        int i = 0;
        _poi = fields[i++];
        _category = fields[i++];
        _subCategory = fields[i++];
        _subSubCategory = fields[i++];
        _name = fields[i++];
        _shortDescription = [fields[i++] stringByReplacingOccurrencesOfString:@";" withString:@","];
        _longDescription = [fields[i++] stringByReplacingOccurrencesOfString:@";" withString:@","];
        _zoneRefID = [NSNumber numberWithInteger: [fields[i++] integerValue]];
        _hours = [fields[i++] stringByReplacingOccurrencesOfString:@";" withString:@","];
        
        NSString * preTag = fields[i];
        preTag = [preTag substringWithRange:NSMakeRange(2, preTag.length-3)];
    
        _tags = [preTag componentsSeparatedByString:@";"];
    }

    return self;
}

@end
